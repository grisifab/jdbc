package exo5;

import java.sql.*;

public class Exo5 {

	public static void main(String[] args) throws ClassNotFoundException {
		// TODO Auto-generated method stub
		String url = "jdbc:mysql://localhost/jdbc?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		String user = "root";
		String password = "root";
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");			
			Connection connexion = DriverManager.getConnection(url, user, password);			
			PreparedStatement ps = connexion.prepareStatement("UPDATE personne SET nom = ?, prenom = ? WHERE num = ?");
			
			ps.setString(1, "Benguigui");
			ps.setString(2, "Morice");
			ps.setInt(3, 10);
			ps.executeUpdate();
			
		}		
		catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
