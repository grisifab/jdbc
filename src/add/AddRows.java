package add;

import java.sql.*;

public class AddRows { //en mode statique

	public static void main(String[] args) throws ClassNotFoundException {
		// TODO Auto-generated method stub
		String url = "jdbc:mysql://localhost/wed?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		String user = "root";
		String password = "root";
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");			
			Connection connexion = DriverManager.getConnection(url, user, password);			
			Statement statement = connexion.createStatement();
			String request = "INSERT INTO wed.user (nom,prenom,sexe,rue,codePostal,ville) VALUES ('Wicko','Johng','C','rue du Sboob', '018254','Tropez');"; 
			int nbr = statement.executeUpdate(request, Statement.RETURN_GENERATED_KEYS);
			if (nbr != 0) {
				System.out.println("insertion reussie " + nbr);
			}
			
			ResultSet resultat = statement.getGeneratedKeys();
			
			if (resultat.next()) {
				System.out.println("num�ro g�n�r� : " + resultat.getInt(1));
			}
		}		
		catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
