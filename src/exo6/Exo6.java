package exo6;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Exo6 {

	public static void main(String[] args) throws ClassNotFoundException {
		// TODO Auto-generated method stub
		String url = "jdbc:mysql://localhost/jdbc?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		String user = "root";
		String password = "root";
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");			
			Connection connexion = DriverManager.getConnection(url, user, password);			
			PreparedStatement ps = connexion.prepareStatement("DELETE FROM personne WHERE num = ?");
			
			ps.setInt(1, 4);
			int rows =ps.executeUpdate();
			System.out.println(rows); // 0 si (ligne inexistante); 1 (si ligne existante et supprim�e)
			
		}		
		catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
