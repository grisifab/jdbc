package exo3;

import java.sql.*;

public class Exo3 { // la m�thode � uitiliser ! ! !

	public static void main(String[] args) throws ClassNotFoundException {
		// TODO Auto-generated method stub
		String url = "jdbc:mysql://localhost/jdbc?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		String user = "root";
		String password = "root";
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");			
			Connection connexion = DriverManager.getConnection(url, user, password);			
			Statement statement = connexion.createStatement();
			String request = "INSERT INTO personne (nom,prenom) VALUES (?,?);"; // ? remplac�s par la suite
			PreparedStatement ps = connexion.prepareStatement(request,PreparedStatement.RETURN_GENERATED_KEYS);
			ps.setString(1,  "Wicky");
			ps.setString(2,  "Johnny");
			ps.executeUpdate();
			ResultSet resultat = ps.getGeneratedKeys();		
			if (resultat.next()) {
				System.out.println("num�ro g�n�r� : " + resultat.getInt(1));
			}
		}		
		catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
