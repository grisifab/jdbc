package exoTrans1;

import java.sql.*;

public class ExoTrans1 { // les transactions permettent de garantir l'intégrité de la base

	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/jdbc?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		String user = "root";
		String passwd = "root";
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection conn = DriverManager.getConnection(url, user, passwd);
			conn.setAutoCommit(false);													// on stocke les commandes
			String sql = "DELETE FROM personne WHERE num = ?";
			PreparedStatement state = conn.prepareStatement(sql);
			state.setInt(1, 12);
			state.executeUpdate();
			ResultSet result = state.executeQuery("SELECT * FROM personne");
			//result.first();
			while (result.next()) {
				System.out.println("Nom : " + result.getString("nom") + " - Prenom : " + result.getString("prenom"));
			}
			conn.commit();																// on les envoies
			result.close();
			state.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}