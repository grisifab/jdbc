package exo4;

import java.sql.*;

public class Exo4 {

	public static void main(String[] args) throws ClassNotFoundException {
		// TODO Auto-generated method stub
		String url = "jdbc:mysql://localhost/jdbc?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		String user = "root";
		String password = "root";
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");			
			Connection connexion = DriverManager.getConnection(url, user, password);			
			Statement statement = connexion.createStatement();
			
			String query = "UPDATE personne SET nom = 'Didi', prenom = 'Jean' WHERE num = 6"; // Attention Statique !
			
			int rowInserted = statement.executeUpdate(query);
			if (rowInserted > 0) {
				System.out.println("update r�ussi");
			}
			statement.close();	
		}		
		catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
