package exo1;

import java.sql.*;

public class Exo1 {

	public static void main(String[] args) throws ClassNotFoundException {
		// TODO Auto-generated method stub
		String url = "jdbc:mysql://localhost/jdbc?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		String user = "root";
		String password = "root";
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");			
			Connection connexion = DriverManager.getConnection(url, user, password);			
			Statement statement = connexion.createStatement();			
			String request = "SELECT * FROM personne";
			ResultSet result = statement.executeQuery(request); // requete de lecture, executeUpdate pour les modifs
			while (result.next()) {				
				int idPersonne = result.getInt("num");
				String nom = result.getString("nom");
				String prenom = result.getString("prenom");
				System.out.println(prenom + " " + nom + " " + idPersonne);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
