package affichage;

import java.sql.*;

public class Affichage {

	public static void main(String[] args) throws ClassNotFoundException {
		// TODO Auto-generated method stub
		String url = "jdbc:mysql://localhost/wed?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		String user = "root";
		String password = "root";
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");			
			Connection connexion = DriverManager.getConnection(url, user, password);			
			Statement statement = connexion.createStatement();			
			String request = "SELECT * FROM user";
			ResultSet result = statement.executeQuery(request); // requete de lecture, executeUpdate pour les modifs
			while (result.next()) {				
				int idPersonne = result.getInt("id");
				String nom = result.getString("nom");
				String prenom = result.getString("prenom");
				String sexe = result.getString("sexe");
				String rue = result.getString("rue");
				String codePostal = result.getString("codePostal");
				String ville = result.getString("ville");			
				System.out.println(prenom + " " + nom + " " + idPersonne + " " + ville);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
