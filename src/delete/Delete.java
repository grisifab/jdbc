package delete;

import java.sql.*;

public class Delete {

	public static void main(String[] args) throws ClassNotFoundException {
		// TODO Auto-generated method stub
		String url = "jdbc:mysql://localhost/wed?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		String user = "root";
		String password = "root";
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");			
			Connection connexion = DriverManager.getConnection(url, user, password);			
			PreparedStatement ps = connexion.prepareStatement("DELETE FROM user WHERE id = ?");
			
			ps.setInt(1, 3);
			int rows =ps.executeUpdate();
			System.out.println(rows); // 0 si (ligne inexistante); 1 (si ligne existante et supprim�e)
			
		}		
		catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
